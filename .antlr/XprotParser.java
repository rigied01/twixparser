// Generated from /home/daverigie/Projects/twixparser/Xprot.g4 by ANTLR 4.7
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class XprotParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, CHAR=17, 
		STRING=18, INT=19, NUMBER=20, BOOL=21, WS=22, BRACKETED_CHARS=23, BURN_PROPERTY=24, 
		BURN_PARAM_CARD_LAYOUT=25, BURN_DEPENDENCY=26;
	public static final int
		RULE_xprot = 0, RULE_node = 1, RULE_param_generic = 2, RULE_param_array = 3, 
		RULE_param_map = 4, RULE_array_value = 5;
	public static final String[] ruleNames = {
		"xprot", "node", "param_generic", "param_array", "param_map", "array_value"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'<XProtocol>'", "'{'", "'<Name>'", "'<ID>'", "'<Userversion>'", 
		"'<'", "'<EVAStringTable>'", "'}'", "'.'", "'>'", "'<ParamArray.'", "'<Default>'", 
		"'<ParamMap.'", "'<Pipe.'", "'<PipeService.'", "'<ParamFunctor.'", null, 
		null, null, null, "'(\"true\"|\"false\")'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "CHAR", "STRING", "INT", "NUMBER", "BOOL", 
		"WS", "BRACKETED_CHARS", "BURN_PROPERTY", "BURN_PARAM_CARD_LAYOUT", "BURN_DEPENDENCY"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Xprot.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public XprotParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class XprotContext extends ParserRuleContext {
		public List<TerminalNode> STRING() { return getTokens(XprotParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(XprotParser.STRING, i);
		}
		public List<TerminalNode> NUMBER() { return getTokens(XprotParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(XprotParser.NUMBER, i);
		}
		public List<NodeContext> node() {
			return getRuleContexts(NodeContext.class);
		}
		public NodeContext node(int i) {
			return getRuleContext(NodeContext.class,i);
		}
		public List<TerminalNode> BURN_PARAM_CARD_LAYOUT() { return getTokens(XprotParser.BURN_PARAM_CARD_LAYOUT); }
		public TerminalNode BURN_PARAM_CARD_LAYOUT(int i) {
			return getToken(XprotParser.BURN_PARAM_CARD_LAYOUT, i);
		}
		public List<TerminalNode> BURN_DEPENDENCY() { return getTokens(XprotParser.BURN_DEPENDENCY); }
		public TerminalNode BURN_DEPENDENCY(int i) {
			return getToken(XprotParser.BURN_DEPENDENCY, i);
		}
		public XprotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_xprot; }
	}

	public final XprotContext xprot() throws RecognitionException {
		XprotContext _localctx = new XprotContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_xprot);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(12);
			match(T__0);
			setState(13);
			match(T__1);
			setState(18);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(14);
				match(T__2);
				setState(15);
				match(STRING);
				}
				}
				setState(20);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(25);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__3) {
				{
				{
				setState(21);
				match(T__3);
				setState(22);
				match(NUMBER);
				}
				}
				setState(27);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(37);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__4) {
				{
				{
				setState(28);
				match(T__4);
				{
				setState(32);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(29);
						_la = _input.LA(1);
						if ( _la <= 0 || (_la==T__5) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						} 
					}
					setState(34);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				}
				}
				}
				}
				setState(39);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(50);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__6) {
				{
				{
				setState(40);
				match(T__6);
				setState(41);
				match(T__1);
				setState(43); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(42);
						_la = _input.LA(1);
						if ( _la <= 0 || (_la==T__5) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(45); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(47);
				match(T__7);
				}
				}
				setState(52);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(54); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(53);
				node();
				}
				}
				setState(56); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__10) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15))) != 0) );
			setState(61);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BURN_PARAM_CARD_LAYOUT) {
				{
				{
				setState(58);
				match(BURN_PARAM_CARD_LAYOUT);
				}
				}
				setState(63);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(67);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BURN_DEPENDENCY) {
				{
				{
				setState(64);
				match(BURN_DEPENDENCY);
				}
				}
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(70);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NodeContext extends ParserRuleContext {
		public Param_mapContext param_map() {
			return getRuleContext(Param_mapContext.class,0);
		}
		public Param_arrayContext param_array() {
			return getRuleContext(Param_arrayContext.class,0);
		}
		public Param_genericContext param_generic() {
			return getRuleContext(Param_genericContext.class,0);
		}
		public NodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_node; }
	}

	public final NodeContext node() throws RecognitionException {
		NodeContext _localctx = new NodeContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_node);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__12:
			case T__13:
			case T__14:
			case T__15:
				{
				setState(72);
				param_map();
				}
				break;
			case T__10:
				{
				setState(73);
				param_array();
				}
				break;
			case T__5:
				{
				setState(74);
				param_generic();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_genericContext extends ParserRuleContext {
		public List<TerminalNode> STRING() { return getTokens(XprotParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(XprotParser.STRING, i);
		}
		public List<TerminalNode> CHAR() { return getTokens(XprotParser.CHAR); }
		public TerminalNode CHAR(int i) {
			return getToken(XprotParser.CHAR, i);
		}
		public List<TerminalNode> BURN_PROPERTY() { return getTokens(XprotParser.BURN_PROPERTY); }
		public TerminalNode BURN_PROPERTY(int i) {
			return getToken(XprotParser.BURN_PROPERTY, i);
		}
		public List<TerminalNode> NUMBER() { return getTokens(XprotParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(XprotParser.NUMBER, i);
		}
		public Param_genericContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_generic; }
	}

	public final Param_genericContext param_generic() throws RecognitionException {
		Param_genericContext _localctx = new Param_genericContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_param_generic);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			match(T__5);
			setState(79); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(78);
				match(CHAR);
				}
				}
				setState(81); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CHAR );
			setState(83);
			match(T__8);
			setState(84);
			match(STRING);
			setState(85);
			match(T__9);
			setState(86);
			match(T__1);
			setState(90);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BURN_PROPERTY) {
				{
				{
				setState(87);
				match(BURN_PROPERTY);
				}
				}
				setState(92);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(96);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==STRING || _la==NUMBER) {
				{
				{
				setState(93);
				_la = _input.LA(1);
				if ( !(_la==STRING || _la==NUMBER) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				}
				setState(98);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(99);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_arrayContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(XprotParser.STRING, 0); }
		public TerminalNode BURN_PROPERTY() { return getToken(XprotParser.BURN_PROPERTY, 0); }
		public NodeContext node() {
			return getRuleContext(NodeContext.class,0);
		}
		public List<Array_valueContext> array_value() {
			return getRuleContexts(Array_valueContext.class);
		}
		public Array_valueContext array_value(int i) {
			return getRuleContext(Array_valueContext.class,i);
		}
		public Param_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_array; }
	}

	public final Param_arrayContext param_array() throws RecognitionException {
		Param_arrayContext _localctx = new Param_arrayContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_param_array);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			match(T__10);
			setState(102);
			match(STRING);
			setState(103);
			match(T__9);
			setState(107);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(104);
				match(T__1);
				}
				}
				setState(109);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(110);
			match(BURN_PROPERTY);
			setState(111);
			match(T__11);
			setState(112);
			node();
			setState(116);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(113);
				array_value();
				}
				}
				setState(118);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(119);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_mapContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(XprotParser.STRING, 0); }
		public List<TerminalNode> BURN_PROPERTY() { return getTokens(XprotParser.BURN_PROPERTY); }
		public TerminalNode BURN_PROPERTY(int i) {
			return getToken(XprotParser.BURN_PROPERTY, i);
		}
		public List<NodeContext> node() {
			return getRuleContexts(NodeContext.class);
		}
		public NodeContext node(int i) {
			return getRuleContext(NodeContext.class,i);
		}
		public Param_mapContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_map; }
	}

	public final Param_mapContext param_map() throws RecognitionException {
		Param_mapContext _localctx = new Param_mapContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_param_map);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(122);
			match(STRING);
			setState(123);
			match(T__9);
			setState(124);
			match(T__1);
			setState(128);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BURN_PROPERTY) {
				{
				{
				setState(125);
				match(BURN_PROPERTY);
				}
				}
				setState(130);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(132); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(131);
				node();
				}
				}
				setState(134); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__10) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15))) != 0) );
			setState(136);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_valueContext extends ParserRuleContext {
		public List<TerminalNode> BURN_PROPERTY() { return getTokens(XprotParser.BURN_PROPERTY); }
		public TerminalNode BURN_PROPERTY(int i) {
			return getToken(XprotParser.BURN_PROPERTY, i);
		}
		public List<Array_valueContext> array_value() {
			return getRuleContexts(Array_valueContext.class);
		}
		public Array_valueContext array_value(int i) {
			return getRuleContext(Array_valueContext.class,i);
		}
		public List<TerminalNode> STRING() { return getTokens(XprotParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(XprotParser.STRING, i);
		}
		public List<TerminalNode> NUMBER() { return getTokens(XprotParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(XprotParser.NUMBER, i);
		}
		public Array_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_value; }
	}

	public final Array_valueContext array_value() throws RecognitionException {
		Array_valueContext _localctx = new Array_valueContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_array_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(138);
			match(T__1);
			setState(142);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BURN_PROPERTY) {
				{
				{
				setState(139);
				match(BURN_PROPERTY);
				}
				}
				setState(144);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(163);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				{
				setState(148);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__1) {
					{
					{
					setState(145);
					array_value();
					}
					}
					setState(150);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				break;
			case 2:
				{
				{
				setState(154);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==STRING) {
					{
					{
					setState(151);
					match(STRING);
					}
					}
					setState(156);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				break;
			case 3:
				{
				{
				setState(160);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NUMBER) {
					{
					{
					setState(157);
					match(NUMBER);
					}
					}
					setState(162);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				break;
			}
			setState(165);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\34\u00aa\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2\3\2\3\2\3\2\7\2\23\n\2\f"+
		"\2\16\2\26\13\2\3\2\3\2\7\2\32\n\2\f\2\16\2\35\13\2\3\2\3\2\7\2!\n\2\f"+
		"\2\16\2$\13\2\7\2&\n\2\f\2\16\2)\13\2\3\2\3\2\3\2\6\2.\n\2\r\2\16\2/\3"+
		"\2\7\2\63\n\2\f\2\16\2\66\13\2\3\2\6\29\n\2\r\2\16\2:\3\2\7\2>\n\2\f\2"+
		"\16\2A\13\2\3\2\7\2D\n\2\f\2\16\2G\13\2\3\2\3\2\3\3\3\3\3\3\5\3N\n\3\3"+
		"\4\3\4\6\4R\n\4\r\4\16\4S\3\4\3\4\3\4\3\4\3\4\7\4[\n\4\f\4\16\4^\13\4"+
		"\3\4\7\4a\n\4\f\4\16\4d\13\4\3\4\3\4\3\5\3\5\3\5\3\5\7\5l\n\5\f\5\16\5"+
		"o\13\5\3\5\3\5\3\5\3\5\7\5u\n\5\f\5\16\5x\13\5\3\5\3\5\3\6\3\6\3\6\3\6"+
		"\3\6\7\6\u0081\n\6\f\6\16\6\u0084\13\6\3\6\6\6\u0087\n\6\r\6\16\6\u0088"+
		"\3\6\3\6\3\7\3\7\7\7\u008f\n\7\f\7\16\7\u0092\13\7\3\7\7\7\u0095\n\7\f"+
		"\7\16\7\u0098\13\7\3\7\7\7\u009b\n\7\f\7\16\7\u009e\13\7\3\7\7\7\u00a1"+
		"\n\7\f\7\16\7\u00a4\13\7\5\7\u00a6\n\7\3\7\3\7\3\7\2\2\b\2\4\6\b\n\f\2"+
		"\5\3\2\b\b\4\2\24\24\26\26\3\2\17\22\2\u00bb\2\16\3\2\2\2\4M\3\2\2\2\6"+
		"O\3\2\2\2\bg\3\2\2\2\n{\3\2\2\2\f\u008c\3\2\2\2\16\17\7\3\2\2\17\24\7"+
		"\4\2\2\20\21\7\5\2\2\21\23\7\24\2\2\22\20\3\2\2\2\23\26\3\2\2\2\24\22"+
		"\3\2\2\2\24\25\3\2\2\2\25\33\3\2\2\2\26\24\3\2\2\2\27\30\7\6\2\2\30\32"+
		"\7\26\2\2\31\27\3\2\2\2\32\35\3\2\2\2\33\31\3\2\2\2\33\34\3\2\2\2\34\'"+
		"\3\2\2\2\35\33\3\2\2\2\36\"\7\7\2\2\37!\n\2\2\2 \37\3\2\2\2!$\3\2\2\2"+
		"\" \3\2\2\2\"#\3\2\2\2#&\3\2\2\2$\"\3\2\2\2%\36\3\2\2\2&)\3\2\2\2\'%\3"+
		"\2\2\2\'(\3\2\2\2(\64\3\2\2\2)\'\3\2\2\2*+\7\t\2\2+-\7\4\2\2,.\n\2\2\2"+
		"-,\3\2\2\2./\3\2\2\2/-\3\2\2\2/\60\3\2\2\2\60\61\3\2\2\2\61\63\7\n\2\2"+
		"\62*\3\2\2\2\63\66\3\2\2\2\64\62\3\2\2\2\64\65\3\2\2\2\658\3\2\2\2\66"+
		"\64\3\2\2\2\679\5\4\3\28\67\3\2\2\29:\3\2\2\2:8\3\2\2\2:;\3\2\2\2;?\3"+
		"\2\2\2<>\7\33\2\2=<\3\2\2\2>A\3\2\2\2?=\3\2\2\2?@\3\2\2\2@E\3\2\2\2A?"+
		"\3\2\2\2BD\7\34\2\2CB\3\2\2\2DG\3\2\2\2EC\3\2\2\2EF\3\2\2\2FH\3\2\2\2"+
		"GE\3\2\2\2HI\7\n\2\2I\3\3\2\2\2JN\5\n\6\2KN\5\b\5\2LN\5\6\4\2MJ\3\2\2"+
		"\2MK\3\2\2\2ML\3\2\2\2N\5\3\2\2\2OQ\7\b\2\2PR\7\23\2\2QP\3\2\2\2RS\3\2"+
		"\2\2SQ\3\2\2\2ST\3\2\2\2TU\3\2\2\2UV\7\13\2\2VW\7\24\2\2WX\7\f\2\2X\\"+
		"\7\4\2\2Y[\7\32\2\2ZY\3\2\2\2[^\3\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]b\3\2\2"+
		"\2^\\\3\2\2\2_a\t\3\2\2`_\3\2\2\2ad\3\2\2\2b`\3\2\2\2bc\3\2\2\2ce\3\2"+
		"\2\2db\3\2\2\2ef\7\n\2\2f\7\3\2\2\2gh\7\r\2\2hi\7\24\2\2im\7\f\2\2jl\7"+
		"\4\2\2kj\3\2\2\2lo\3\2\2\2mk\3\2\2\2mn\3\2\2\2np\3\2\2\2om\3\2\2\2pq\7"+
		"\32\2\2qr\7\16\2\2rv\5\4\3\2su\5\f\7\2ts\3\2\2\2ux\3\2\2\2vt\3\2\2\2v"+
		"w\3\2\2\2wy\3\2\2\2xv\3\2\2\2yz\7\n\2\2z\t\3\2\2\2{|\t\4\2\2|}\7\24\2"+
		"\2}~\7\f\2\2~\u0082\7\4\2\2\177\u0081\7\32\2\2\u0080\177\3\2\2\2\u0081"+
		"\u0084\3\2\2\2\u0082\u0080\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0086\3\2"+
		"\2\2\u0084\u0082\3\2\2\2\u0085\u0087\5\4\3\2\u0086\u0085\3\2\2\2\u0087"+
		"\u0088\3\2\2\2\u0088\u0086\3\2\2\2\u0088\u0089\3\2\2\2\u0089\u008a\3\2"+
		"\2\2\u008a\u008b\7\n\2\2\u008b\13\3\2\2\2\u008c\u0090\7\4\2\2\u008d\u008f"+
		"\7\32\2\2\u008e\u008d\3\2\2\2\u008f\u0092\3\2\2\2\u0090\u008e\3\2\2\2"+
		"\u0090\u0091\3\2\2\2\u0091\u00a5\3\2\2\2\u0092\u0090\3\2\2\2\u0093\u0095"+
		"\5\f\7\2\u0094\u0093\3\2\2\2\u0095\u0098\3\2\2\2\u0096\u0094\3\2\2\2\u0096"+
		"\u0097\3\2\2\2\u0097\u00a6\3\2\2\2\u0098\u0096\3\2\2\2\u0099\u009b\7\24"+
		"\2\2\u009a\u0099\3\2\2\2\u009b\u009e\3\2\2\2\u009c\u009a\3\2\2\2\u009c"+
		"\u009d\3\2\2\2\u009d\u00a6\3\2\2\2\u009e\u009c\3\2\2\2\u009f\u00a1\7\26"+
		"\2\2\u00a0\u009f\3\2\2\2\u00a1\u00a4\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a2"+
		"\u00a3\3\2\2\2\u00a3\u00a6\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a5\u0096\3\2"+
		"\2\2\u00a5\u009c\3\2\2\2\u00a5\u00a2\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7"+
		"\u00a8\7\n\2\2\u00a8\r\3\2\2\2\30\24\33\"\'/\64:?EMS\\bmv\u0082\u0088"+
		"\u0090\u0096\u009c\u00a2\u00a5";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}