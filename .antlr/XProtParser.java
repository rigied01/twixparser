// Generated from /home/daverigie/Projects/twixparser/XProtParser.g4 by ANTLR 4.7
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class XProtParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WHITESPACE=1, NUMBER_VALUE=2, BOOL=3, ASCCONV_STRING=4, QUOTED_STRING=5, 
		BURN_PROPERTY=6, BURN_PARAM_CARD_LAYOUT=7, BURN_DEPENDENCY=8, BRA=9, KET=10, 
		LEFT_BRACE=11, RIGHT_BRACE=12, DEFAULT=13, ERROR_CHARACTER=14, PARAM_MAP_TAG_TYPE=15, 
		PARAM_ARRAY_TAG_TYPE=16, XPROTOCOL=17, SPECIAL_TAG_TYPE=18, EVASTRINGTABLE=19, 
		TAG_TYPE=20, TAG_DOT=21, TAG_NAME=22, EVA_WHITESPACE=23, STRING_TABLE=24;
	public static final int
		RULE_header = 0, RULE_xprot = 1, RULE_xprot_tag = 2, RULE_special_node = 3, 
		RULE_node = 4, RULE_param_special = 5, RULE_param_special_tag = 6, RULE_param_eva = 7, 
		RULE_param_eva_tag = 8, RULE_param_array = 9, RULE_param_array_tag = 10, 
		RULE_param_map = 11, RULE_param_map_tag = 12, RULE_array_value = 13, RULE_arr_val_item = 14, 
		RULE_param_generic = 15, RULE_param_generic_val = 16, RULE_param_generic_tag = 17;
	public static final String[] ruleNames = {
		"header", "xprot", "xprot_tag", "special_node", "node", "param_special", 
		"param_special_tag", "param_eva", "param_eva_tag", "param_array", "param_array_tag", 
		"param_map", "param_map_tag", "array_value", "arr_val_item", "param_generic", 
		"param_generic_val", "param_generic_tag"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, "'(\"true\"|\"false\")'", null, null, null, null, null, 
		"'<'", null, "'{'", "'}'", "'<Default>'", null, null, "'ParamArray'", 
		"'XProtocol'", null, "'EVAStringTable'", null, "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "WHITESPACE", "NUMBER_VALUE", "BOOL", "ASCCONV_STRING", "QUOTED_STRING", 
		"BURN_PROPERTY", "BURN_PARAM_CARD_LAYOUT", "BURN_DEPENDENCY", "BRA", "KET", 
		"LEFT_BRACE", "RIGHT_BRACE", "DEFAULT", "ERROR_CHARACTER", "PARAM_MAP_TAG_TYPE", 
		"PARAM_ARRAY_TAG_TYPE", "XPROTOCOL", "SPECIAL_TAG_TYPE", "EVASTRINGTABLE", 
		"TAG_TYPE", "TAG_DOT", "TAG_NAME", "EVA_WHITESPACE", "STRING_TABLE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "XProtParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public XProtParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class HeaderContext extends ParserRuleContext {
		public List<XprotContext> xprot() {
			return getRuleContexts(XprotContext.class);
		}
		public XprotContext xprot(int i) {
			return getRuleContext(XprotContext.class,i);
		}
		public List<TerminalNode> ASCCONV_STRING() { return getTokens(XProtParser.ASCCONV_STRING); }
		public TerminalNode ASCCONV_STRING(int i) {
			return getToken(XProtParser.ASCCONV_STRING, i);
		}
		public HeaderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_header; }
	}

	public final HeaderContext header() throws RecognitionException {
		HeaderContext _localctx = new HeaderContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_header);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(39);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BRA) {
				{
				{
				setState(36);
				xprot();
				}
				}
				setState(41);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(45);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ASCCONV_STRING) {
				{
				{
				setState(42);
				match(ASCCONV_STRING);
				}
				}
				setState(47);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class XprotContext extends ParserRuleContext {
		public Xprot_tagContext xprot_tag() {
			return getRuleContext(Xprot_tagContext.class,0);
		}
		public TerminalNode LEFT_BRACE() { return getToken(XProtParser.LEFT_BRACE, 0); }
		public TerminalNode RIGHT_BRACE() { return getToken(XProtParser.RIGHT_BRACE, 0); }
		public List<Special_nodeContext> special_node() {
			return getRuleContexts(Special_nodeContext.class);
		}
		public Special_nodeContext special_node(int i) {
			return getRuleContext(Special_nodeContext.class,i);
		}
		public List<NodeContext> node() {
			return getRuleContexts(NodeContext.class);
		}
		public NodeContext node(int i) {
			return getRuleContext(NodeContext.class,i);
		}
		public List<TerminalNode> BURN_DEPENDENCY() { return getTokens(XProtParser.BURN_DEPENDENCY); }
		public TerminalNode BURN_DEPENDENCY(int i) {
			return getToken(XProtParser.BURN_DEPENDENCY, i);
		}
		public XprotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_xprot; }
	}

	public final XprotContext xprot() throws RecognitionException {
		XprotContext _localctx = new XprotContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_xprot);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(48);
			xprot_tag();
			setState(49);
			match(LEFT_BRACE);
			setState(53);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(50);
					special_node();
					}
					} 
				}
				setState(55);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(59);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BURN_PARAM_CARD_LAYOUT || _la==BRA) {
				{
				{
				setState(56);
				node();
				}
				}
				setState(61);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(65);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BURN_DEPENDENCY) {
				{
				{
				setState(62);
				match(BURN_DEPENDENCY);
				}
				}
				setState(67);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(68);
			match(RIGHT_BRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Xprot_tagContext extends ParserRuleContext {
		public TerminalNode BRA() { return getToken(XProtParser.BRA, 0); }
		public TerminalNode XPROTOCOL() { return getToken(XProtParser.XPROTOCOL, 0); }
		public TerminalNode KET() { return getToken(XProtParser.KET, 0); }
		public Xprot_tagContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_xprot_tag; }
	}

	public final Xprot_tagContext xprot_tag() throws RecognitionException {
		Xprot_tagContext _localctx = new Xprot_tagContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_xprot_tag);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			match(BRA);
			setState(71);
			match(XPROTOCOL);
			setState(72);
			match(KET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Special_nodeContext extends ParserRuleContext {
		public Param_specialContext param_special() {
			return getRuleContext(Param_specialContext.class,0);
		}
		public Param_evaContext param_eva() {
			return getRuleContext(Param_evaContext.class,0);
		}
		public Special_nodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_special_node; }
	}

	public final Special_nodeContext special_node() throws RecognitionException {
		Special_nodeContext _localctx = new Special_nodeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_special_node);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				{
				setState(74);
				param_special();
				}
				break;
			case 2:
				{
				setState(75);
				param_eva();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NodeContext extends ParserRuleContext {
		public Param_mapContext param_map() {
			return getRuleContext(Param_mapContext.class,0);
		}
		public Param_arrayContext param_array() {
			return getRuleContext(Param_arrayContext.class,0);
		}
		public Param_genericContext param_generic() {
			return getRuleContext(Param_genericContext.class,0);
		}
		public TerminalNode BURN_PARAM_CARD_LAYOUT() { return getToken(XProtParser.BURN_PARAM_CARD_LAYOUT, 0); }
		public NodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_node; }
	}

	public final NodeContext node() throws RecognitionException {
		NodeContext _localctx = new NodeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_node);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(82);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(78);
				param_map();
				}
				break;
			case 2:
				{
				setState(79);
				param_array();
				}
				break;
			case 3:
				{
				setState(80);
				param_generic();
				}
				break;
			case 4:
				{
				setState(81);
				match(BURN_PARAM_CARD_LAYOUT);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_specialContext extends ParserRuleContext {
		public Param_special_tagContext param_special_tag() {
			return getRuleContext(Param_special_tagContext.class,0);
		}
		public TerminalNode QUOTED_STRING() { return getToken(XProtParser.QUOTED_STRING, 0); }
		public TerminalNode NUMBER_VALUE() { return getToken(XProtParser.NUMBER_VALUE, 0); }
		public Param_specialContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_special; }
	}

	public final Param_specialContext param_special() throws RecognitionException {
		Param_specialContext _localctx = new Param_specialContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_param_special);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(84);
			param_special_tag();
			setState(85);
			_la = _input.LA(1);
			if ( !(_la==NUMBER_VALUE || _la==QUOTED_STRING) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_special_tagContext extends ParserRuleContext {
		public TerminalNode BRA() { return getToken(XProtParser.BRA, 0); }
		public TerminalNode SPECIAL_TAG_TYPE() { return getToken(XProtParser.SPECIAL_TAG_TYPE, 0); }
		public TerminalNode KET() { return getToken(XProtParser.KET, 0); }
		public Param_special_tagContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_special_tag; }
	}

	public final Param_special_tagContext param_special_tag() throws RecognitionException {
		Param_special_tagContext _localctx = new Param_special_tagContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_param_special_tag);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			match(BRA);
			setState(88);
			match(SPECIAL_TAG_TYPE);
			setState(89);
			match(KET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_evaContext extends ParserRuleContext {
		public Param_eva_tagContext param_eva_tag() {
			return getRuleContext(Param_eva_tagContext.class,0);
		}
		public TerminalNode LEFT_BRACE() { return getToken(XProtParser.LEFT_BRACE, 0); }
		public TerminalNode STRING_TABLE() { return getToken(XProtParser.STRING_TABLE, 0); }
		public TerminalNode RIGHT_BRACE() { return getToken(XProtParser.RIGHT_BRACE, 0); }
		public Param_evaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_eva; }
	}

	public final Param_evaContext param_eva() throws RecognitionException {
		Param_evaContext _localctx = new Param_evaContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_param_eva);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(91);
			param_eva_tag();
			setState(92);
			match(LEFT_BRACE);
			setState(93);
			match(STRING_TABLE);
			setState(94);
			match(RIGHT_BRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_eva_tagContext extends ParserRuleContext {
		public TerminalNode BRA() { return getToken(XProtParser.BRA, 0); }
		public TerminalNode EVASTRINGTABLE() { return getToken(XProtParser.EVASTRINGTABLE, 0); }
		public TerminalNode KET() { return getToken(XProtParser.KET, 0); }
		public Param_eva_tagContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_eva_tag; }
	}

	public final Param_eva_tagContext param_eva_tag() throws RecognitionException {
		Param_eva_tagContext _localctx = new Param_eva_tagContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_param_eva_tag);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			match(BRA);
			setState(97);
			match(EVASTRINGTABLE);
			setState(98);
			match(KET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_arrayContext extends ParserRuleContext {
		public Param_array_tagContext param_array_tag() {
			return getRuleContext(Param_array_tagContext.class,0);
		}
		public TerminalNode LEFT_BRACE() { return getToken(XProtParser.LEFT_BRACE, 0); }
		public TerminalNode DEFAULT() { return getToken(XProtParser.DEFAULT, 0); }
		public NodeContext node() {
			return getRuleContext(NodeContext.class,0);
		}
		public TerminalNode RIGHT_BRACE() { return getToken(XProtParser.RIGHT_BRACE, 0); }
		public List<Array_valueContext> array_value() {
			return getRuleContexts(Array_valueContext.class);
		}
		public Array_valueContext array_value(int i) {
			return getRuleContext(Array_valueContext.class,i);
		}
		public Param_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_array; }
	}

	public final Param_arrayContext param_array() throws RecognitionException {
		Param_arrayContext _localctx = new Param_arrayContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_param_array);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			param_array_tag();
			setState(101);
			match(LEFT_BRACE);
			setState(102);
			match(DEFAULT);
			setState(103);
			node();
			setState(107);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LEFT_BRACE) {
				{
				{
				setState(104);
				array_value();
				}
				}
				setState(109);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(110);
			match(RIGHT_BRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_array_tagContext extends ParserRuleContext {
		public TerminalNode BRA() { return getToken(XProtParser.BRA, 0); }
		public TerminalNode PARAM_ARRAY_TAG_TYPE() { return getToken(XProtParser.PARAM_ARRAY_TAG_TYPE, 0); }
		public TerminalNode TAG_DOT() { return getToken(XProtParser.TAG_DOT, 0); }
		public TerminalNode TAG_NAME() { return getToken(XProtParser.TAG_NAME, 0); }
		public TerminalNode KET() { return getToken(XProtParser.KET, 0); }
		public Param_array_tagContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_array_tag; }
	}

	public final Param_array_tagContext param_array_tag() throws RecognitionException {
		Param_array_tagContext _localctx = new Param_array_tagContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_param_array_tag);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(112);
			match(BRA);
			setState(113);
			match(PARAM_ARRAY_TAG_TYPE);
			setState(114);
			match(TAG_DOT);
			setState(115);
			match(TAG_NAME);
			setState(116);
			match(KET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_mapContext extends ParserRuleContext {
		public Param_map_tagContext param_map_tag() {
			return getRuleContext(Param_map_tagContext.class,0);
		}
		public TerminalNode LEFT_BRACE() { return getToken(XProtParser.LEFT_BRACE, 0); }
		public TerminalNode RIGHT_BRACE() { return getToken(XProtParser.RIGHT_BRACE, 0); }
		public List<NodeContext> node() {
			return getRuleContexts(NodeContext.class);
		}
		public NodeContext node(int i) {
			return getRuleContext(NodeContext.class,i);
		}
		public Param_mapContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_map; }
	}

	public final Param_mapContext param_map() throws RecognitionException {
		Param_mapContext _localctx = new Param_mapContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_param_map);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			param_map_tag();
			setState(119);
			match(LEFT_BRACE);
			setState(123);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BURN_PARAM_CARD_LAYOUT || _la==BRA) {
				{
				{
				setState(120);
				node();
				}
				}
				setState(125);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(126);
			match(RIGHT_BRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_map_tagContext extends ParserRuleContext {
		public TerminalNode BRA() { return getToken(XProtParser.BRA, 0); }
		public TerminalNode PARAM_MAP_TAG_TYPE() { return getToken(XProtParser.PARAM_MAP_TAG_TYPE, 0); }
		public TerminalNode TAG_DOT() { return getToken(XProtParser.TAG_DOT, 0); }
		public TerminalNode TAG_NAME() { return getToken(XProtParser.TAG_NAME, 0); }
		public TerminalNode KET() { return getToken(XProtParser.KET, 0); }
		public Param_map_tagContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_map_tag; }
	}

	public final Param_map_tagContext param_map_tag() throws RecognitionException {
		Param_map_tagContext _localctx = new Param_map_tagContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_param_map_tag);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(128);
			match(BRA);
			setState(129);
			match(PARAM_MAP_TAG_TYPE);
			setState(130);
			match(TAG_DOT);
			setState(131);
			match(TAG_NAME);
			setState(132);
			match(KET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_valueContext extends ParserRuleContext {
		public TerminalNode LEFT_BRACE() { return getToken(XProtParser.LEFT_BRACE, 0); }
		public TerminalNode RIGHT_BRACE() { return getToken(XProtParser.RIGHT_BRACE, 0); }
		public List<Arr_val_itemContext> arr_val_item() {
			return getRuleContexts(Arr_val_itemContext.class);
		}
		public Arr_val_itemContext arr_val_item(int i) {
			return getRuleContext(Arr_val_itemContext.class,i);
		}
		public Array_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_value; }
	}

	public final Array_valueContext array_value() throws RecognitionException {
		Array_valueContext _localctx = new Array_valueContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_array_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(134);
			match(LEFT_BRACE);
			setState(138);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUMBER_VALUE) | (1L << QUOTED_STRING) | (1L << LEFT_BRACE))) != 0)) {
				{
				{
				setState(135);
				arr_val_item();
				}
				}
				setState(140);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(141);
			match(RIGHT_BRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Arr_val_itemContext extends ParserRuleContext {
		public Arr_val_itemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arr_val_item; }
	 
		public Arr_val_itemContext() { }
		public void copyFrom(Arr_val_itemContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AnArrayValContext extends Arr_val_itemContext {
		public Array_valueContext array_value() {
			return getRuleContext(Array_valueContext.class,0);
		}
		public AnArrayValContext(Arr_val_itemContext ctx) { copyFrom(ctx); }
	}
	public static class NumberContext extends Arr_val_itemContext {
		public TerminalNode NUMBER_VALUE() { return getToken(XProtParser.NUMBER_VALUE, 0); }
		public NumberContext(Arr_val_itemContext ctx) { copyFrom(ctx); }
	}
	public static class StringContext extends Arr_val_itemContext {
		public TerminalNode QUOTED_STRING() { return getToken(XProtParser.QUOTED_STRING, 0); }
		public StringContext(Arr_val_itemContext ctx) { copyFrom(ctx); }
	}

	public final Arr_val_itemContext arr_val_item() throws RecognitionException {
		Arr_val_itemContext _localctx = new Arr_val_itemContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_arr_val_item);
		try {
			setState(146);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case QUOTED_STRING:
				_localctx = new StringContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(143);
				match(QUOTED_STRING);
				}
				break;
			case NUMBER_VALUE:
				_localctx = new NumberContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(144);
				match(NUMBER_VALUE);
				}
				break;
			case LEFT_BRACE:
				_localctx = new AnArrayValContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(145);
				array_value();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_genericContext extends ParserRuleContext {
		public Param_generic_tagContext param_generic_tag() {
			return getRuleContext(Param_generic_tagContext.class,0);
		}
		public TerminalNode LEFT_BRACE() { return getToken(XProtParser.LEFT_BRACE, 0); }
		public TerminalNode RIGHT_BRACE() { return getToken(XProtParser.RIGHT_BRACE, 0); }
		public List<Param_generic_valContext> param_generic_val() {
			return getRuleContexts(Param_generic_valContext.class);
		}
		public Param_generic_valContext param_generic_val(int i) {
			return getRuleContext(Param_generic_valContext.class,i);
		}
		public Param_genericContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_generic; }
	}

	public final Param_genericContext param_generic() throws RecognitionException {
		Param_genericContext _localctx = new Param_genericContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_param_generic);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(148);
			param_generic_tag();
			setState(149);
			match(LEFT_BRACE);
			setState(153);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUMBER_VALUE) | (1L << ASCCONV_STRING) | (1L << QUOTED_STRING))) != 0)) {
				{
				{
				setState(150);
				param_generic_val();
				}
				}
				setState(155);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(156);
			match(RIGHT_BRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_generic_valContext extends ParserRuleContext {
		public TerminalNode ASCCONV_STRING() { return getToken(XProtParser.ASCCONV_STRING, 0); }
		public TerminalNode QUOTED_STRING() { return getToken(XProtParser.QUOTED_STRING, 0); }
		public TerminalNode NUMBER_VALUE() { return getToken(XProtParser.NUMBER_VALUE, 0); }
		public Param_generic_valContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_generic_val; }
	}

	public final Param_generic_valContext param_generic_val() throws RecognitionException {
		Param_generic_valContext _localctx = new Param_generic_valContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_param_generic_val);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(158);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUMBER_VALUE) | (1L << ASCCONV_STRING) | (1L << QUOTED_STRING))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_generic_tagContext extends ParserRuleContext {
		public TerminalNode BRA() { return getToken(XProtParser.BRA, 0); }
		public TerminalNode TAG_TYPE() { return getToken(XProtParser.TAG_TYPE, 0); }
		public TerminalNode KET() { return getToken(XProtParser.KET, 0); }
		public TerminalNode TAG_DOT() { return getToken(XProtParser.TAG_DOT, 0); }
		public TerminalNode TAG_NAME() { return getToken(XProtParser.TAG_NAME, 0); }
		public Param_generic_tagContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_generic_tag; }
	}

	public final Param_generic_tagContext param_generic_tag() throws RecognitionException {
		Param_generic_tagContext _localctx = new Param_generic_tagContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_param_generic_tag);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			match(BRA);
			setState(161);
			match(TAG_TYPE);
			setState(164);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TAG_DOT) {
				{
				setState(162);
				match(TAG_DOT);
				setState(163);
				match(TAG_NAME);
				}
			}

			setState(166);
			match(KET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\32\u00ab\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\3\2\7\2(\n\2\f\2\16\2+\13\2\3\2\7\2.\n\2\f\2\16\2\61\13\2\3"+
		"\3\3\3\3\3\7\3\66\n\3\f\3\16\39\13\3\3\3\7\3<\n\3\f\3\16\3?\13\3\3\3\7"+
		"\3B\n\3\f\3\16\3E\13\3\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\5\5O\n\5\3\6\3"+
		"\6\3\6\3\6\5\6U\n\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3"+
		"\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\7\13l\n\13\f\13\16\13o\13\13\3"+
		"\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\7\r|\n\r\f\r\16\r\177\13"+
		"\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\7\17\u008b\n\17\f\17"+
		"\16\17\u008e\13\17\3\17\3\17\3\20\3\20\3\20\5\20\u0095\n\20\3\21\3\21"+
		"\3\21\7\21\u009a\n\21\f\21\16\21\u009d\13\21\3\21\3\21\3\22\3\22\3\23"+
		"\3\23\3\23\3\23\5\23\u00a7\n\23\3\23\3\23\3\23\2\2\24\2\4\6\b\n\f\16\20"+
		"\22\24\26\30\32\34\36 \"$\2\4\4\2\4\4\7\7\4\2\4\4\6\7\2\u00a8\2)\3\2\2"+
		"\2\4\62\3\2\2\2\6H\3\2\2\2\bN\3\2\2\2\nT\3\2\2\2\fV\3\2\2\2\16Y\3\2\2"+
		"\2\20]\3\2\2\2\22b\3\2\2\2\24f\3\2\2\2\26r\3\2\2\2\30x\3\2\2\2\32\u0082"+
		"\3\2\2\2\34\u0088\3\2\2\2\36\u0094\3\2\2\2 \u0096\3\2\2\2\"\u00a0\3\2"+
		"\2\2$\u00a2\3\2\2\2&(\5\4\3\2\'&\3\2\2\2(+\3\2\2\2)\'\3\2\2\2)*\3\2\2"+
		"\2*/\3\2\2\2+)\3\2\2\2,.\7\6\2\2-,\3\2\2\2.\61\3\2\2\2/-\3\2\2\2/\60\3"+
		"\2\2\2\60\3\3\2\2\2\61/\3\2\2\2\62\63\5\6\4\2\63\67\7\r\2\2\64\66\5\b"+
		"\5\2\65\64\3\2\2\2\669\3\2\2\2\67\65\3\2\2\2\678\3\2\2\28=\3\2\2\29\67"+
		"\3\2\2\2:<\5\n\6\2;:\3\2\2\2<?\3\2\2\2=;\3\2\2\2=>\3\2\2\2>C\3\2\2\2?"+
		"=\3\2\2\2@B\7\n\2\2A@\3\2\2\2BE\3\2\2\2CA\3\2\2\2CD\3\2\2\2DF\3\2\2\2"+
		"EC\3\2\2\2FG\7\16\2\2G\5\3\2\2\2HI\7\13\2\2IJ\7\23\2\2JK\7\f\2\2K\7\3"+
		"\2\2\2LO\5\f\7\2MO\5\20\t\2NL\3\2\2\2NM\3\2\2\2O\t\3\2\2\2PU\5\30\r\2"+
		"QU\5\24\13\2RU\5 \21\2SU\7\t\2\2TP\3\2\2\2TQ\3\2\2\2TR\3\2\2\2TS\3\2\2"+
		"\2U\13\3\2\2\2VW\5\16\b\2WX\t\2\2\2X\r\3\2\2\2YZ\7\13\2\2Z[\7\24\2\2["+
		"\\\7\f\2\2\\\17\3\2\2\2]^\5\22\n\2^_\7\r\2\2_`\7\32\2\2`a\7\16\2\2a\21"+
		"\3\2\2\2bc\7\13\2\2cd\7\25\2\2de\7\f\2\2e\23\3\2\2\2fg\5\26\f\2gh\7\r"+
		"\2\2hi\7\17\2\2im\5\n\6\2jl\5\34\17\2kj\3\2\2\2lo\3\2\2\2mk\3\2\2\2mn"+
		"\3\2\2\2np\3\2\2\2om\3\2\2\2pq\7\16\2\2q\25\3\2\2\2rs\7\13\2\2st\7\22"+
		"\2\2tu\7\27\2\2uv\7\30\2\2vw\7\f\2\2w\27\3\2\2\2xy\5\32\16\2y}\7\r\2\2"+
		"z|\5\n\6\2{z\3\2\2\2|\177\3\2\2\2}{\3\2\2\2}~\3\2\2\2~\u0080\3\2\2\2\177"+
		"}\3\2\2\2\u0080\u0081\7\16\2\2\u0081\31\3\2\2\2\u0082\u0083\7\13\2\2\u0083"+
		"\u0084\7\21\2\2\u0084\u0085\7\27\2\2\u0085\u0086\7\30\2\2\u0086\u0087"+
		"\7\f\2\2\u0087\33\3\2\2\2\u0088\u008c\7\r\2\2\u0089\u008b\5\36\20\2\u008a"+
		"\u0089\3\2\2\2\u008b\u008e\3\2\2\2\u008c\u008a\3\2\2\2\u008c\u008d\3\2"+
		"\2\2\u008d\u008f\3\2\2\2\u008e\u008c\3\2\2\2\u008f\u0090\7\16\2\2\u0090"+
		"\35\3\2\2\2\u0091\u0095\7\7\2\2\u0092\u0095\7\4\2\2\u0093\u0095\5\34\17"+
		"\2\u0094\u0091\3\2\2\2\u0094\u0092\3\2\2\2\u0094\u0093\3\2\2\2\u0095\37"+
		"\3\2\2\2\u0096\u0097\5$\23\2\u0097\u009b\7\r\2\2\u0098\u009a\5\"\22\2"+
		"\u0099\u0098\3\2\2\2\u009a\u009d\3\2\2\2\u009b\u0099\3\2\2\2\u009b\u009c"+
		"\3\2\2\2\u009c\u009e\3\2\2\2\u009d\u009b\3\2\2\2\u009e\u009f\7\16\2\2"+
		"\u009f!\3\2\2\2\u00a0\u00a1\t\3\2\2\u00a1#\3\2\2\2\u00a2\u00a3\7\13\2"+
		"\2\u00a3\u00a6\7\26\2\2\u00a4\u00a5\7\27\2\2\u00a5\u00a7\7\30\2\2\u00a6"+
		"\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a9\7\f"+
		"\2\2\u00a9%\3\2\2\2\17)/\67=CNTm}\u008c\u0094\u009b\u00a6";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}