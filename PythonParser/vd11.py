import numpy as np

uint8  = np.uint8
uint16 = np.uint16
uint32 = np.uint32
uint64 = np.uint64
uchar  = np.string_

filepath = "/media/h/Projects/twixparser/data/meas_MID00917_FID33651_tse_nyu_nav.dat"


MrParcRaidFileHeader = np.dtype([('hdSize',uint32),
                                 ('count',uint32)])

MrParcRaidFileEntry  = np.dtype([('measid',uint32),
                                 ('fileId',uint32),
                                 ('off',uint64),
                                 ('len',uint64),
                                 ('patName', uchar, 64),
                                 ('protName', uchar, 64)])

MultiRaidFileHeader = np.dtype([('MrParcRaidFileHeader',MrParcRaidFileHeader, 1),
                                ('MrParcRaidFileEntry', MrParcRaidFileEntry, 64)])

Align504 = np.dtype([('',uint8, 504)])
Align512 = np.dtype([('',uint8, 512)])

def read_until_null(f):
    bytearr = []
    byte = 1
    null_byte = 0
    while byte != null_byte:
        byte = np.fromfile(f, uint8, 1)[0]
        bytearr.append(byte)

    return np.array(bytearr[0:-1])

def read_meas_header(f):
    header_size = np.fromfile(f, uint32, 1)[0]
    num_buffers = np.fromfile(f, uint32, 1)[0]

    # Look for name of first buffer stops at NULL character?
    buffers = dict()
    for i in range(num_buffers):
        bufname = read_until_null(f).tostring().decode('utf-8')
        buflen = np.fromfile(f, np.uint32, 1)[0]
        buf = np.fromfile(f, np.uint8, buflen).tostring().decode('utf-8')
        buffers[bufname] = buf   

    return buffers, header_size, num_buffers

def read_scan(f):

    ScanHeader    = np.dtype([('DMALength', uint32, 1),
                              ('MeasUID', uint32, 1),
                              ('ScanCounter', uint32, 1),
                              ('TimeStamp', uint32, 1),
                              ('PMUTimeStamp', uint32, 1),
                              ('SystemType', uint16, 1),
                              ('PTABPosDelay', uint16, 1),
                              ('PTABPosX', uint32, 1),
                              ('PTAPosY', uint32, 1),
                              ('PTAPosZ', uint32, 1),
                              ('Reserved1', uint32, 1),
                              ('EvalInfoMask', uint32, 2),
                              ('SamplesInScan', uint16, 1),
                              ('UsedChannel', uint16, 1),
                              ('sLoopCounter', uint32, 7),
                              ('sCutOffData', uint32, 1),
                              ('KSpaceCentreColumn', uint16, 1),
                              ('CoilSelect', uint16, 1),
                              ('ReadOutOffcentre', uint32, 1),
                              ('TimeSinceLastRF', uint32, 1),
                              ('KSpaceCentreLineNo', uint16, 1),
                              ('KSpaceCentrePartitionNo', uint16, 1),
                              ('SliceData', uint8, 7),
                              ('IceProgramPara', uint8, 48),
                              ('ReservedPara', uint8, 8),
                              ('ApplicationCounter', uint16, 1),
                              ('ApplicationMask', uint16, 1),
                              ('CRC', uint32, 1)])

    ChannelHeader = np.dtype([('TypeAndChannelLenght', uint32, 1),
                              ('MeasUID', uint32, 1),
                              ('ScanCounter', uint32, 1),
                              ('Reserved1', uint32, 1),
                              ('SequenceTime', uint32, 1),
                              ('Unused2', uint32, 1),
                              ('ChannelId', uint16, 1),
                              ('Unused3', uint16, 1),
                              ('CRC', uint32, 1)])

    
    scan_header = np.fromfile(f, ScanHeader, 1)

    num_samples  = scan_header[0]['SamplesInScan'][0]
    num_channels = scan_header[0]['UsedChannel'][0] 

    MeasData = np.dtype([('ChannelHeader', ChannelHeader, 1), 
                         ('pixel_data', uint32, num_samples*2)])

    channel_data = np.fromfile(f, MeasData, num_channels)

    return channel_data

    



with open(filepath, "rb") as f:
    
    bytes  = np.fromfile(f, MultiRaidFileHeader, 1)[0]
    np.fromfile(f, Align504, 1)
    pos = f.tell()
    buffers, header_size, num_buffers = read_meas_header(f)
    #print(f.tell())
    #f.seek(pos, 0)
    #print(f.tell())
    #f.seek(header_size, 1)
    #print(f.tell())
    #print(header_size)
    #channel_data = read_scan(f)

    ScanHeader    = np.dtype([('DMALength', uint32, 1),
                              ('MeasUID', uint32, 1),
                              ('ScanCounter', uint32, 1),
                              ('TimeStamp', uint32, 1),
                              ('PMUTimeStamp', uint32, 1),
                              ('SystemType', uint16, 1),
                              ('PTABPosDelay', uint16, 1),
                              ('PTABPosX', uint32, 1),
                              ('PTAPosY', uint32, 1),
                              ('PTAPosZ', uint32, 1),
                              ('Reserved1', uint32, 1),
                              ('EvalInfoMask', uint32, 2),
                              ('SamplesInScan', uint16, 1),
                              ('UsedChannel', uint16, 1),
                              ('sLoopCounter', uint32, 7),
                              ('sCutOffData', uint32, 1),
                              ('KSpaceCentreColumn', uint16, 1),
                              ('CoilSelect', uint16, 1),
                              ('ReadOutOffcentre', uint32, 1),
                              ('TimeSinceLastRF', uint32, 1),
                              ('KSpaceCentreLineNo', uint16, 1),
                              ('KSpaceCentrePartitionNo', uint16, 1),
                              ('SliceData', uint32, 7),
                              ('IceProgramPara', uint8, 48),
                              ('ReservedPara', uint8, 8),
                              ('ApplicationCounter', uint16, 1),
                              ('ApplicationMask', uint16, 1),
                              ('CRC', uint32, 1)])

    ChannelHeader = np.dtype([('TypeAndChannelLength', uint32, 1),
                              ('MeasUID', uint32, 1),
                              ('ScanCounter', uint32, 1),
                              ('Reserved1', uint32, 1),
                              ('SequenceTime', uint32, 1),
                              ('Unused2', uint32, 1),
                              ('ChannelId', uint16, 1),
                              ('Unused3', uint16, 1),
                              ('CRC', uint32, 1)])

    p1 = f.tell()
    scan_header = np.fromfile(f, ScanHeader, 1)
    print(f.tell()-p1)

    num_samples  = scan_header['SamplesInScan'][0]
    num_channels = scan_header['UsedChannel'][0] 

    MeasData = np.dtype([('ChannelHeader', ChannelHeader, 1), 
                         ('pixel_data', uint32, num_samples*2)])

    channel_data = np.fromfile(f, MeasData, num_channels)