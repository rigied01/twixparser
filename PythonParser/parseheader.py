####
# Only working on compatibility with VD for now

import numpy as np
import re
import yaml, json
import os
import sys

def mkdir_for_file(filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            pass

def dict2yaml(d, filename):
    mkdir_for_file(filename)
    with open(filename, 'w+') as f:
        yaml.dump(json.loads(json.dumps(d)), f, allow_unicode=True, default_flow_style=False)
        
def dict2json(d, filename):
    mkdir_for_file(filename)
    with open(filename, 'w+') as f:
        s = json.dumps(d, indent=4)
        string2file(s, filename)

def string2file(s, filename):
    mkdir_for_file(filename)
    with open(filename, "w+") as fid:
        fid.write("{0}".format(s))

