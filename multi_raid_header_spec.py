from utils import *
import numpy as np

@assert_size_in_bytes(9736)
def MULTI_RAID_FILE_HEADER(): 
    return \
    np.dtype(
        [
            ('mr_parc_raid_file_header', MR_PARC_RAID_FILE_HEADER(), 1),
            ('mr_parc_raid_file_entry',  MR_PARC_RAID_FILE_ENTRY(),  64)
        ]
    )

@assert_size_in_bytes(8)
def MR_PARC_RAID_FILE_HEADER(): 
    return \
    np.dtype(
        [
            ('header_size', 'u4', 1),
            ('num_meas',    'u4', 1)
        ]
    )

@assert_size_in_bytes(152)
def MR_PARC_RAID_FILE_ENTRY():
    return \
    np.dtype(
        [
            # fieldName       dtype         count
            # =========       =====         =====
            ('meas_id',       'u4',          1),
            ('file_id',       'u4',          1),
            ('offset',        'u8',          1),
            ('length',        'u8',          1),
            ('patient_name',   np.bytes_,    64),
            ('protocol_name',  np.bytes_,    64)
        ]
    )