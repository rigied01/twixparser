# What is this?

I will keep some notes for myself here based on reverse engineering the MATLAB code mapVBVD and looking through the header text.

There seem to be two types of structures in the header that are parsed separately in mapVBVD. 

# Header Structure

## XProtocol

The name of the root node is given by a set of characters preceding the <XProtocol> tag. There seem to be other NULL characters etc also, so these need to be somehow ignored.

Below this appears to be some kind of nested set of tags. However, mapVBVD collapses the tree structure into a single flat map by just looking for parameters which are of type: DOUBLE, BOOL, LONG, or STRING

The values are found using regular expressions

## ASCCONV
The name of this buffer is given by a set of characters in the beginning and ending with a null character.

Each line is a C-like declaration of a variable/struct 

I need to figure out how to convert this to some Python compatible data structure


