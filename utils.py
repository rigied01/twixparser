from warnings import warn
import numpy as np
import numpy.core.numerictypes as nt
import time
import ipdb
from io import StringIO

try :
    from colorama import Fore
except:
    warn('colorama not installed, no worries!')
    
    class ForeDummy:
        def __getattribute__(self, attr):
            return ''

    Fore = ForeDummy()

NULL_BYTE = b'\x00'

def read_null_terminated_string(f, max_len=128):
    string = b''
    next_char = b''
    while next_char != NULL_BYTE:
        string = string + next_char
        next_char = f.read(1) 
        assert len(string) < max_len
    
    return string

def generate_bitfield(n):
    bf = 1 << np.arange(n)
    return bf

def eval_bitfield(x, bitfield):
    return (x & bitfield) > 0

class assert_size_in_bytes:

    def __init__(self, byte_size):
        self.byte_size = byte_size
    
    def __call__(self, f):
        def wrapped_f(*args,**kwargs):
            val  = f(*args, **kwargs)
            assert  type(val) is np.dtype, \
            'expected numpy.dtype not {0}'.format(type(val))

            assert  val.itemsize == self.byte_size,\
            'expected {0} bytes, found {1} bytes'.format(self.byte_size, 
                                                    val.itemsize)
        

            return val

        return wrapped_f

def arr2str(x):
    outstring = ''
 
    for name in x.dtype.names:
        val = x[name]
        if type(val) is np.void:
            val = bytes(val)
        if type(val) is bytes:
            valstring = '{0}B bytestring'.format(len(val))
        else:
            valstring = str(val)

        outstring = outstring + Fore.LIGHTBLUE_EX+name+': ' + Fore.RESET + valstring + '\n'
    
    return outstring

def print_arr(x):

    print(arr2str(x))

def prettyvars(obj, skip_private = True):
    d = vars(obj)
    for key,val in d.items():
        if key[0] == '_':
            continue
        print(Fore.LIGHTBLUE_EX + key + ': ' + Fore.RESET + str(val))

def print_table(data, headers=None, col_width=20):
    io = StringIO()
    num_cols = len(data[0])
    fmt = '{:>' + str(col_width) + '}'
    if headers:
        print(Fore.LIGHTBLUE_EX + \
              (fmt*num_cols).format(*headers, col_width=col_width) + \
              Fore.RESET, \
              file=io
        )
        

    for row in data:
        val_strings = tuple(map(str, row))
        print((fmt*num_cols).format(*val_strings, col_width=col_width), file=io)

    io.seek(0)
    s = io.read()
    print(s)
    return s

def ensure_not_none(x, default_val):
    if x is None:
        return default_val
    return x

def has_len(x):
    try:
        len(x)
        return True
    except:
        return False